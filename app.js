const express = require('express')

const app = express()

// set up view engine
app.set('view engine', 'ejs')

// create home route
app.get('/', (res, req) => {
  res.render('home')
})

app.listen(8080, () => {
  console.log('app now listening for requests on port 8080')
})
