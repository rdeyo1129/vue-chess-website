import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import Campaign from '@/components/Campaign'
import Profile from '@/components/Profile'
import Teams from '@/components/Teams'
import Reference from '@/components/Reference'
import Training from '@/components/Training'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/campaign',
      name: 'Campaign',
      component: Campaign
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/teams',
      name: 'Teams',
      component: Teams
    },
    {
      path: '/reference',
      name: 'Reference',
      component: Reference
    },
    {
      path: '/training',
      name: 'Training',
      component: Training
    }
  ]
})
